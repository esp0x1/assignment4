# Assignment 4
Noroff Accelerate Assignment #4, Full-stack .NET development Javascript
```
Creating a dynamic webpage using JS
```

# Libraries used
## [axios](https://axios-http.com/docs/intro)
## [bootstrap 5 css](https://getbootstrap.com/docs/5.0/getting-started/introduction/)

# Contributors
## Espen Sjo | Bergen, Norway

</br>

# Project Structure
```
.
└── Assignment4/
    ├── index.js
    └── index.html

```
