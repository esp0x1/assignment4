//
// Laptop fetch
//
const getLaptops = async () => {
    try{
        const response = await axios.get('https://noroff-komputer-store-api.herokuapp.com/computers')
        return response
    } catch(error){
        console.log(error)
    }
}

(async () => {
    //
    // Initalize objects
    //
    const bankAccount = {
        "id": 1,
        "Name": "Frank the bank",
        "balance": 200,
        "loan": 0,
    }

    const work = {
        'pay': 0,
    }

    //
    // DOM - Bank Section
    //
    const elBankAccountLoanBalanceDiv = document.getElementById('bank-account-info')
    const elBankAccountName = document.getElementById('bank-account-name')
    const elBankAccountBalance = document.getElementById('bank-balance')
    const elBankAccountGetLoanBtn = document.getElementById('get-loan-btn')
    let elBankAccountLoanBalanceSpan = null

    //
    // Work Section DOM
    //
    const elWorkBankBtn = document.getElementById('work-bank-btn')
    const elWorkPayBtn = document.getElementById('work-pay-btn')
    const elWorkPayBalance = document.getElementById('work-balance')
    const elWorkRepayLoanBtn = document.getElementById('repay-loan-btn')

    //
    // Laptop section DOM
    //
    const elLaptopSelect = document.getElementById("laptop-select")
    const elLaptopImage = document.getElementById("laptop-image")
    const elLaptopDesc = document.getElementById("laptop-description")
    const elLaptopSpecs = document.getElementById("laptop-specs")
    const elLaptopBuyTitle = document.getElementById("buy-laptop-title")
    const elLaptopSpecTitle = document.getElementById("laptop-spec-title")
    const elLaptopBuyPrice = document.getElementById("buy-laptop-price")
    const elLaptopBuyBtn = document.getElementById("buy-laptop-button")
    
    //
    // initial event listeners
    //
    elBankAccountGetLoanBtn.addEventListener("click", onLoanBtnClick)
    elWorkBankBtn.addEventListener("click", onBankBtnClick)
    elWorkPayBtn.addEventListener("click", onWorkBtnClick)
    elLaptopBuyBtn.addEventListener("click", onBuyBtnClick)
    elLaptopSelect.addEventListener("change", onSelectChange)
    elWorkRepayLoanBtn.addEventListener("click", onRepayLoanClick)
  
    
    //
    // Render Initial items
    //
    elBankAccountName.innerText = bankAccount.Name
    renderBankAccountBalance()
    renderPayBalance()
    const laptops = await getLaptops()
    laptops.data.forEach( (laptop) =>  {
        elLaptopSelect.innerHTML += `<option value=${laptop.id}>${laptop.title}</option>`
    })

    //
    // Bank section Handlers
    //
    function renderBankAccountBalance(){
        elBankAccountBalance.innerText = bankAccount.balance
    }
    
    function renderLoanBalance(){
        document.getElementById("bank-loan-balance").innerText = bankAccount.loan
    }

    function removeLoanBalance(){
        elBankAccountLoanBalanceSpan.remove()
    }

    function onLoanBtnClick(){
        if(bankAccount.loan > 0){
            alert("Please repay your current loan first!")
            return
        }
    
        let loanAmount = prompt("Please Enter The Desired Loan Amount")
    
        // ensure prompt input is valid
        if(isNaN(loanAmount) || loanAmount <= 0){
            alert("Invalid input, please enter a valid number!")
            onLoanBtnClick();
        }
        else if( (bankAccount.balance * 2) < loanAmount){
            alert("Invalid loan amount, please only loan what you're able to pay back!")
            onLoanBtnClick();
        }
        else{
            // append '+' to LoanAmount to force number formatting
            bankAccount.balance += +loanAmount
            bankAccount.loan = +loanAmount
    
            if(document.getElementById("bank-loan-balance") == null){
                createLoanBalance();
            }
    
            renderBankAccountBalance();
            renderLoanBalance();
            //CreatePayLoanButton();
            elWorkRepayLoanBtn.hidden = false
        }
    }
    //func to create loan balance span and p text
    function createLoanBalance(){
        let LoanBalanceSpan = document.createElement("span")
        LoanBalanceSpan.setAttribute("id", "bank-loan-balance-span")
        LoanBalanceSpan.innerText = "Loan"
    
        let LoanBalance = document.createElement("p")
        LoanBalance.setAttribute("id", "bank-loan-balance")
        LoanBalance.InnerText = bankAccount.loan
    
        LoanBalanceSpan.appendChild(LoanBalance)
        elBankAccountLoanBalanceDiv.appendChild(LoanBalanceSpan)
    
        elBankAccountLoanBalanceSpan = document.getElementById("bank-loan-balance-span")
    }

    //
    // WORK Section Handlers
    //
    function renderPayBalance(){
        elWorkPayBalance.innerText = work.pay
    }
    // Bank Button, Work Section
    function onBankBtnClick(){
        // if loan exists
        if(bankAccount.loan > 0){
            // calculate downpayment
            const paidDown = 0.1 * work.pay
            // remove sum of payment from pay and from the loan
            work.pay -= paidDown
            bankAccount.loan -= paidDown
            // if loan is negative, too much has been taken from pay, add it back; set loan to 0
            if(bankAccount.loan <= 0){
                work.pay += Math.abs(bankAccount.loan)
                bankAccount.loan = 0
                elWorkRepayLoanBtn.hidden = true
                removeLoanBalance()
            }
            // ensure we are not attempting to render if account has no loan
            renderLoanBalance()
        }
        // append to balance, reset pay, render
        bankAccount.balance += work.pay
        work.pay = 0
        renderPayBalance()
        renderBankAccountBalance()
    }
    function onWorkBtnClick(){
        work.pay += 100
        renderPayBalance()
    }

    // Event Listener function for Pay Back 100% of pay balance towards loan
    function onRepayLoanClick(){
        bankAccount.loan -= work.pay
        // if loan in negative, return excess paid loan
        if(bankAccount.loan <= 0){
           work.pay = Math.abs(bankAccount.loan)
           bankAccount.loan = 0
           elWorkRepayLoanBtn.hidden = true
           bankAccount.balance += work.pay
           work.pay = 0
           removeLoanBalance()
           renderPayBalance()
           renderBankAccountBalance()
           return;
        }
        work.pay = 0
        renderPayBalance()
        renderLoanBalance()
    }

    //
    //laptop handlers
    //
    function renderLaptopInformation(laptop){
        // edge case, no laptop is selected after selecting one
        if(typeof laptop === 'undefined'){
            elLaptopImage.src = ""
            elLaptopImage.onerror = undefined
            elLaptopSpecs.innerHTML = ""
            elLaptopDesc.innerText = ""
            elLaptopBuyTitle.innerText = ""
            elLaptopSpecTitle.innerText = ""
            elLaptopBuyPrice.innerText = ""
            elLaptopBuyBtn.hidden = true
            return;
        }
        elLaptopImage.src = 'https://noroff-komputer-store-api.herokuapp.com/' + laptop.image
        elLaptopImage.onerror = () =>{elLaptopImage.src='https://img.freepik.com/premium-vector/folder-found_22110-498.jpg?w=826'}
        elLaptopSpecs.innerHTML = ""
        elLaptopDesc.innerText = laptop.description
        elLaptopBuyTitle.innerText = laptop.title
        elLaptopSpecTitle.innerText = "Specifications"
        elLaptopBuyPrice.innerText = laptop.price + " NOK"
        elLaptopBuyBtn.hidden = false
        elLaptopBuyBtn.value = laptop.id

        laptop.specs.forEach( (spec) =>{
            elLaptopSpecs.innerHTML += `<li>${spec}</li>`
        })
    }
    function onSelectChange(){
        const laptopId = this.value
        const selectedLaptop = laptops.data.find(l => l.id === +laptopId)
       
        renderLaptopInformation(selectedLaptop)
    }
    
    function onBuyBtnClick(){
        const laptopId = this.value 
        const laptop = laptops.data.find(l => l.id === +laptopId)
        if(bankAccount.balance < laptop.price){
            alert("Your balance is too low! Please come back when you have earned " + (laptop.price - bankAccount.balance) + " NOK")
            return
        }
        bankAccount.balance -= laptop.price
        renderBankAccountBalance()
        alert('You  are now the owner of a brand new ' + laptop.title)
        
    }

    
})()
